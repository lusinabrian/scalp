package com.scalp.datastructures.arrays.buildarrayfromperm

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class BuildArrayFromPermTest extends AnyFlatSpec with Matchers {
  "BuildArray" should "return [0, 1, 2, 4, 5, 3] from [0, 2, 1, 5, 3, 4]" in {
    val nums = Array(0, 2, 1, 5, 3, 4)
    val expected = Array(0, 1, 2, 4, 5, 3)
    val actual = BuildArrayFromPerm.buildArray(nums)

    actual should be(expected)
  }

  "BuildArray" should "return [4, 5, 0, 1, 2, 3] from [5, 0, 1, 2, 3, 4]" in {
    val nums = Array(5, 0, 1, 2, 3, 4)
    val expected = Array(4, 5, 0, 1, 2, 3)
    val actual = BuildArrayFromPerm.buildArray(nums)

    actual should be(expected)
  }
}
