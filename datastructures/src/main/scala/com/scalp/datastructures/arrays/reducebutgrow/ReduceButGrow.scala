package com.scalp.datastructures.arrays.reducebutgrow

object ReduceButGrow {
  def grow(xs: List[Long]): Long = xs.product
}
