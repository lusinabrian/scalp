package com.scalp.datastructures.arrays.buildarrayfromperm


object BuildArrayFromPerm {
  def buildArray(nums: Array[Int]): Array[Int] = {
    val ans = new Array[Int](nums.length)

    for (i <- nums.indices) {
      ans(i) = nums(nums(i))
    }

    ans
  }
}
