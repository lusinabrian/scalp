# Scalp

Implementation of various algorithms in Scala programming language & some snippets

## How to use this repository for learning purpose?

- [Download and install Scala](https://www.scala-lang.org/download/)
- Clone the repository on your local machine
- Open the repository using an IDE (Preferably IntelliJ)

- Enable auto-import to import the dependencies from build.sbt
- Open the [src/main/scalp](algorithms/src/main/scala/scalp) to view the algorithm implementations under various categories
- Open [src/test/scalp](algorithms/src/test/scala/scalp) to run the test cases and view the algorithm in action

## How to contribute to this repository?

## Contributing Algorithms : (Make sure to check all the points below before opening a PR)

- Make sure you follow proper coding standards and naming conventions.
- Add the algorithm implementation as an object and not as a class
- Add proper comments about the algorithm implementation and the functions present in the algorithm
- Add a test spec in the test folder under the same domain as that of your algorithm. PR without a test spec would not be accepted
- Add at least 1 positive and 1 negative test in the test spec
- Raise a PR for the algorithm object file and the test spec
- [How to write a test case in Scala](http://www.scalatest.org/user_guide/writing_your_first_test)

## Contributing Tests

- You can contribute tests for a specific algorithm
- Add as many tests as you can and try to cover all the border line test cases
- Open a PR with for your testSpec
- Make sure you are not adding redundant test cases
- If you see that the algorithm fails for a particular test case, then open an issue with proper explanation.
- [How to write a test case in Scala](http://www.scalatest.org/user_guide/writing_your_first_test)
  